﻿using SEData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEBluePrintPlanner
{
    public partial class Form1 : Form
    {
        public static Form1 Instance;

        private Grid gridType = Grid.None;

        private SEMaterialLoader _materialLoader;

        private SEComponentLoader _componenetLoader;

        private SEBlockLoader _blockLoader;

        private List<Tuple<SEBlock, int>> _bluePrintCollectionItems = new List<Tuple<SEBlock, int>>();

        private List<Tuple<SEComponent, int>> componentsReq = new List<Tuple<SEComponent, int>>();


        public Form1()
        {
            InitializeComponent();

            Instance = this;

            // set the grid choice

            comboGrid.Items.Add("None");

            comboGrid.Items.Add("Large");

            comboGrid.Items.Add("Small");

            comboGrid.SelectedIndexChanged += ComboGrid_SelectedIndexChanged;

            comboGrid.SelectedIndex = 0;

            comboBlocks.SelectedIndexChanged += ComboBlocks_SelectedIndexChanged;

            txt_Total.TextChanged += Txt_Total_TextChanged;

            LoadFileTest();
        }

        private void Txt_Total_TextChanged(object sender, EventArgs e)
        {
            int amt = 0;

            if (!int.TryParse(txt_Total.Text, out amt))
                txt_Total.Clear();
        }

        private void ComboBlocks_SelectedIndexChanged(object sender, EventArgs e)
        {
            // gonna write req to screen

            SEBlock block = comboBlocks.SelectedItem as SEBlock;

          //  _rtfOutput.Clear();
          //  WriteLine("Needs:");
          //  WriteLine("");
           // WriteLine("");

            foreach(var comp in block.ComponentsRequired)
            {
              //  WriteLine(string.Format("{0} : {1}", comp.Component.Name, comp.Amount));
            }
        }

        private void ComboGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            string grid = comboGrid.SelectedItem.ToString();

            if (grid == "Small")
            {
                var small = _blockLoader.GetBlocksByGridType(Grid.Small).OrderBy(n => n.Name);

                comboBlocks.Items.Clear();

                foreach (var block in small)
                    comboBlocks.Items.Add(block);

                comboBlocks.SelectedIndex = 0;
            }
            else if (grid == "Large")
            {
                var large = _blockLoader.GetBlocksByGridType(Grid.Large).OrderBy(n=>n.Name);

                comboBlocks.Items.Clear();

                foreach (var block in large)
                    comboBlocks.Items.Add(block);

                comboBlocks.SelectedIndex = 0;
            }
            else comboBlocks.Items.Clear();
        }

        public void LoadFileTest()
        {
            try
            {
                _materialLoader = new SEMaterialLoader();

                bool loaded = _materialLoader.LoadFile();

                if(loaded)
                {
                    _componenetLoader = new SEComponentLoader(_materialLoader);

                    bool loadC = _componenetLoader.LoadFile();

                    if(loadC)
                    {
                        _blockLoader = new SEBlockLoader(_componenetLoader);

                        _blockLoader.OnErrorMessage += BLoader_OnErrorMessage;

                        bool bLoaded = _blockLoader.LoadFile();

                        comboBlocks.Items.Clear();

                        comboGrid.SelectedItem = "Large";

                        var blocks =_blockLoader.GetBlocksByGridType(Grid.Large);

                        foreach(var block in blocks)
                        {
                            comboBlocks.Items.Add(block);
                        }

                        comboBlocks.SelectedIndex = 0;
                    }
                }
            }
            catch(Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void BLoader_OnErrorMessage(string error, Type fromClass)
        {
            MessageBox.Show(string.Format("ERROR: {0} FROM: {1}", error, fromClass));
        }

        public void WriteLine(string message)
        {
            if(_rtfOutput.InvokeRequired)
            {
                _rtfOutput.Invoke((MethodInvoker)delegate
                {
                    _rtfOutput.AppendText(message + Environment.NewLine);

                    _rtfOutput.ScrollToCaret();
                });
            }
            else
            {
                _rtfOutput.AppendText(message + Environment.NewLine);

                _rtfOutput.ScrollToCaret();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFileTest();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            // test our amt first
            int amt = 0;

            if(!int.TryParse(txt_Total.Text, out amt))
            {
                MessageBox.Show(string.Format("Unable to add {0} as numeric amount !", txt_Total.Text));

                return;
            }
            // get the block

            SEBlock block = comboBlocks.SelectedItem as SEBlock;

            foreach(var b in block.ComponentsRequired)
            {
               // b.Amount = b.Amount * amt;
            }
            BluePrintCollectionItem item = new BluePrintCollectionItem(block, amt);

            AddBluePrintItem(item, amt);
        }

        private void AddBluePrintItem(BluePrintCollectionItem item, int total)
        {
            foreach(var comp in item.Block.ComponentsRequired)
            {
                int index = componentsReq.FindIndex(c=>c.Item1.ComponentID == comp.Component.ComponentID);

                if (index != -1)
                {
                    int amt = comp.Amount * total;

                    Tuple<SEComponent, int> t = new Tuple<SEComponent, int>(componentsReq[index].Item1, componentsReq[index].Item2 + amt);

                    componentsReq[index] = t;
                }
                else componentsReq.Add(new Tuple<SEComponent, int>(comp.Component, comp.Amount * total));
            }

            _bluePrintCollectionItems.Add(new Tuple<SEBlock, int>(item.Block, total));


            UpdateOutput();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            componentsReq.Clear();

            _rtfOutput.Clear();
        }
        private void UpdateOutput()
        {
            StringBuilder b = new StringBuilder();

            foreach (var c in componentsReq)
            {
                b.AppendLine(string.Format("{0} : {1}", c.Item1, c.Item2));
            }

            _rtfOutput.Clear();

            WriteLine(b.ToString());

            WriteLine(GenerateMaterialsRequired());
        }
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            int amt = 0;

            if (!int.TryParse(txt_Total.Text, out amt))
            {
                MessageBox.Show(string.Format("Unable to add {0} as numeric amount !", txt_Total.Text));

                return;
            }

            // remove what ?

            SEBlock block = comboBlocks.SelectedItem as SEBlock;

            foreach(var comp in block.ComponentsRequired)
            {
                int index = componentsReq.FindIndex(c=>c.Item1.ComponentID == comp.Component.ComponentID);

                if (index != -1)
                {
                    int totalToRemove = comp.Amount * amt;

                    if ((componentsReq[index].Item2 - totalToRemove) > 0)
                    {
                        Tuple<SEComponent, int> newVal = new Tuple<SEComponent, int>(comp.Component, componentsReq[index].Item2 - totalToRemove);

                        componentsReq[index] = newVal;
                    }
                    else componentsReq.RemoveAt(index);
                }
            }

            UpdateOutput();
        }

        private string GenerateMaterialsRequired()
        {
            List<Tuple<SEMaterial, float>> matList = new List<Tuple<SEMaterial, float>>();

            StringBuilder result = new StringBuilder();

            foreach(var comp in componentsReq)
            {
                foreach(var mat in comp.Item1.MaterialsRequired)
                {
                    int index = matList.FindIndex(m=>m.Item1.MaterialID == mat.Material.MaterialID);

                    if (index != -1)
                    {
                        Tuple<SEMaterial, float> m = new Tuple<SEMaterial, float>(matList[index].Item1, matList[index].Item2 + comp.Item2);


                        matList[index] = m;
                    }
                    else
                    {
                        Tuple<SEMaterial, float> m = new Tuple<SEMaterial, float>(mat.Material, mat.Amount * (float)comp.Item2);

                        matList.Add(m);
                    }
                }
            }

            foreach (var d in matList)
            {
                result.AppendLine(string.Format("{0} : {1}", d.Item1.Name, d.Item2));
            }

            return result.ToString();
        }

        private void addToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(_rtfOutput.Text))
                Clipboard.SetText(_rtfOutput.Text);
        }

        private void cancelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            outputMenu.Close();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            componentsReq.Clear();

            _rtfOutput.Clear();
        }

        private void dToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XmlEditorForm x = new XmlEditorForm();

            x.Show();
        }
    }
}
