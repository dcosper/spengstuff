﻿using SEData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEBluePrintPlanner
{
    public partial class XmlEditorForm : Form
    {
        int matImageIndex = 0;

        int matImageSelectedIndex = 1;

        int compImageIndex = 2;

        int compImageSelectedIndex = 3;

        int blockImageIndex = 4;

        int blockImageSelectedIndex = 5;

        private SEMaterialLoader _materialLoader;

        private SEComponentLoader _compLoader;

        private SEBlockLoader _blockLoader;

        public XmlEditorForm()
        {
            InitializeComponent();

            InitializeTree();
        }

        private void InitializeTree()
        {
            TreeNode materialNode = new TreeNode("SEMaterials.xml", matImageIndex, matImageSelectedIndex);

            TreeNode compNode = new TreeNode("SEComps.xml", compImageIndex, compImageSelectedIndex);

            TreeNode blockNode = new TreeNode("SEBlocks.xml", blockImageIndex, blockImageSelectedIndex);

            treeView1.Nodes.Add(materialNode);

            treeView1.Nodes.Add(compNode);

            treeView1.Nodes.Add(blockNode);

            treeView1.AfterSelect += TreeView1_AfterSelect;

            // load our files
            _materialLoader = new SEMaterialLoader();

            _compLoader = new SEComponentLoader(_materialLoader);

            _blockLoader = new SEBlockLoader(_compLoader);

            bool mat = _materialLoader.LoadFile();

            bool comp = _compLoader.LoadFile();

            bool blocks = _blockLoader.LoadFile();

            if (!mat || !comp || !blocks)
                MessageBox.Show("Error Loading files !!");
        }

        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // we need to populate our tabcontrol
            richCodeOutput.Clear();

            switch(e.Node.Index)
            {
                case 0: // materials
                    {
                        tabControl.TabPages[0].Text = "SEMaterials.xml";

                        tabControl.TabPages[1].Text = "Code";

                        richCodeOutput.Text = _materialLoader.GetXMLText();

                        // turn off all the group boxes and then just turn on materials one

                        if (!groupBoxMaterials.Enabled)
                            groupBoxMaterials.Enabled = true;

                    }
                    break;
                case 1: // comps
                    {
                        tabControl.TabPages[0].Text = "SEComps.xml";

                        tabControl.TabPages[1].Text = "Code";

                        richCodeOutput.Text = _compLoader.GetXMLText();
                    }
                    break;
                case 2: // blocks
                    {
                        tabControl.TabPages[0].Text = "SEBlocks.xml";

                        tabControl.TabPages[1].Text = "Code";

                        richCodeOutput.Text = _blockLoader.GetXMLText();
                    }
                    break;
            }
        }
    }
}
