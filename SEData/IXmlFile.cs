﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEData
{
    public interface IXmlFile
    {
        bool LoadFile();
        bool SaveFile();
    }
}
