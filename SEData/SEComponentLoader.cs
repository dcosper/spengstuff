﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEData
{
    public class SEComponentLoader : XmlFileLoader
    {
        public List<SEComponent> Components;

        protected SEMaterialLoader MaterialLoader;

        public SEComponentLoader(SEMaterialLoader materialLoader)
        :base("XmlData/SEComps.xml")
        {
            this.Components = new List<SEComponent>();

            this.MaterialLoader = materialLoader;
        }

        public override bool LoadFile()
        {
            bool loaded = base.LoadFile();

            if (!loaded) return false;

            try
            {
                this.Components.Clear();

                var comps = _baseDocument.Root.Element("Components").Elements("Component");

                foreach(var node in comps)
                {
                    SEComponent comp = new SEComponent();

                    comp.ComponentID = node.Attribute("ID").Value;

                    comp.Name = node.Attribute("Name").Value;

                    comp.MaterialsRequired = new List<SEMaterialsRequired>();

                    foreach(var childNode in node.Element("Materials").Elements("Material"))
                    {
                        if(MaterialLoader != null)
                        {
                            SEMaterial mat = MaterialLoader.GetMaterialByID(childNode.Element("MaterialID").Value);

                            float amt = 0.00f;

                            float.TryParse(childNode.Element("RequiredAmount").Value, out amt);

                            SEMaterialsRequired req = new SEMaterialsRequired(mat, amt);

                            comp.MaterialsRequired.Add(req);
                        }
                    }

                    this.Components.Add(comp);
                }

                return true;
            }
            catch { return false; }
        }

        public bool TryGetComponentByID(string id, out SEComponent component)
        {
            component = null;

            try
            {
                component = Components.Find(c=>c.ComponentID == id);

                return true;
            }
            catch { return false; }
        }
    }
    public class SEComponent
    {
        public string ComponentID { get; set; }

        public string Name { get; set; }

        public List<SEMaterialsRequired> MaterialsRequired;

        public override string ToString()
        {
            return Name;
        }
    }
    public class SEMaterialsRequired
    {
        public SEMaterial Material { get; set; }

        public float Amount { get; set; }

        public SEMaterialsRequired(SEMaterial material, float amt)
        {
            this.Material = material;

            this.Amount = amt;
        }
    }
}
