﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEData
{
    public class SEBlockLoader : XmlFileLoader
    {
        protected SEComponentLoader _compLoader;

        public List<SEBlock> Blocks;

        public event ErrorMessage OnErrorMessage;

        public SEBlockLoader(SEComponentLoader comps)
            :base("XmlData/SEBlocks.xml")
        {
            _compLoader = comps;

            this.Blocks = new List<SEBlock>();
        }

        public override bool LoadFile()
        {
            bool loaded = base.LoadFile();

            if (!loaded) return false;

            this.Blocks.Clear();

            try
            {
                var blocks = _baseDocument.Root.Element("Blocks").Elements("Block");

                foreach(var block in blocks)
                {
                    SEBlock seBlock = new SEBlock();

                    seBlock.BlockID = block.Attribute("ID").Value;

                    seBlock.Name = block.Attribute("Name").Value;

                    string gridType = block.Attribute("GRID").Value;

                    if (gridType == "Large")
                        seBlock.GridType = Grid.Large;
                    else if (gridType == "Small")
                        seBlock.GridType = Grid.Small;
                    else seBlock.GridType = Grid.None;


                    seBlock.ComponentsRequired = new List<SEComponentsRequired>();

                    foreach(var childNode in block.Element("Components").Elements("Component"))
                    {
                        int amt = -1;

                        SEComponent comp = null;

                        bool foundComp = _compLoader.TryGetComponentByID(childNode.Attribute("ID").Value, out comp);

                        int.TryParse(childNode.Value, out amt);

                        SEComponentsRequired req = new SEComponentsRequired(comp, amt);

                        seBlock.ComponentsRequired.Add(req);
                    }

                    this.Blocks.Add(seBlock);
                }

                return true;
            }
            catch(Exception ex) 
            {
                OnErrorMessage?.Invoke(ex.Message + " " + ex.StackTrace + " " + ex.Data, this.GetType());

                return false; 
            }
        }

        public bool TryGetBlockByID(string id, Grid gridType, out SEBlock block)
        {
            block = null;

            try 
            {
                block = Blocks.Find(b=>b.BlockID == id && b.GridType == gridType);

                return true;
            }
            catch { return false; }
        }

        public List<SEBlock> GetBlocksByGridType(Grid gridType)
        {
            return Blocks.Where(g => g.GridType == gridType).OrderBy(n=>n.Name).ToList();
        }
    }
    public class SEBlock
    {
        public string BlockID { get; set; }

        public string Name { get; set; }

        public Grid GridType = Grid.None;

        public List<SEComponentsRequired> ComponentsRequired { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
    public class SEComponentsRequired
    {
        public SEComponent Component { get; set; }

        public int Amount { get; set; }

        public SEComponentsRequired(SEComponent component, int amt)
        {
            this.Component = component;

            this.Amount = amt;
        }
    }
    
    public enum Grid
    {
        None,
        Large,
        Small
    }
}
