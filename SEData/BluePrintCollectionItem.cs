﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEData
{
    public class BluePrintCollectionItem
    {
        public SEBlock Block { get; set; }

        public int Amount { get; set; }

        public BluePrintCollectionItem(SEBlock block, int amt)
        {
            this.Block = block;

            this.Amount = amt;
        }
    }
}
