﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEData
{
    public class SEMaterialLoader : XmlFileLoader
    {
        public List<SEMaterial> Materials;

        public SEMaterialLoader()
            : base("XmlData/SEMaterials.xml")
        {
            this.Materials = new List<SEMaterial>();
        }
        public override bool LoadFile()
        {
            bool fileLoaded = base.LoadFile();

            if (!fileLoaded) return false;

            this.Materials.Clear();

            try
            {
                var mats = _baseDocument.Root.Element("Materials").Elements("Material");

                foreach (var node in mats)
                {
                    SEMaterial m = new SEMaterial();

                    m.MaterialID = node.Attribute("ID").Value;

                    m.Name = node.Element("MaterialName").Value;

                    m.Symbol = node.Element("MaterialSymbol").Value;

                    this.Materials.Add(m);
                }

                return true;
            }
            catch { return false; }
        }
        public SEMaterial GetMaterialByID(string id)
        {
            return Materials.Find(m => m.MaterialID == id);
        }
        public bool TryGetMaterialByID(string id, out SEMaterial material)
        {
            material = null;

            try
            {
                material = Materials.Find(m => m.MaterialID == id);

                return true;
            }
            catch { return false; }
        }
    }
    public class SEMaterial
    {
        public string MaterialID { get; set; }

        public string Name { get; set; }

        public string Symbol { get; set; }
    }
}
