﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SEData
{
    public abstract class XmlFileLoader : IXmlFile
    {
        protected XDocument _baseDocument;

        protected string _documentName;

        public XmlFileLoader(string fileName)
        {
            _documentName = fileName;
        }

        public virtual bool LoadFile()
        {
            if (!File.Exists(_documentName))
                return false;

            try
            {
                _baseDocument = XDocument.Load(_documentName);

                return true;
            }
            catch { return false; }
        }

        public virtual bool SaveFile()
        {
            try
            {
                _baseDocument.Save(_documentName);

                return true;
            }
            catch { return false; }
        }
        public string GetXMLText()
        {
            return _baseDocument.ToString();
        }
    }
}
