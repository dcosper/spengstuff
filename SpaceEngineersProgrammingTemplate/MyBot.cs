﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.ModAPI.Ingame;
using VRage;
using VRage.Game.ModAPI.Ingame;

namespace SpaceEngineersProgrammingTemplate
{
    public class MyBot : MyGridProgram
    {
        #region CODE
        // we want the remote
        // we want batteries
        // we want sensors
        // we want carogs
        // we want ore detector
        // we want antenna
        // we want gyros
        // we want other programming blocks ?
        // we want thrusters, hydro, ion and atmo

        // if we go drone
        // arm rotors
        // drill or welder or grinder
        IMyRemoteControl remoteControl = null;

        IMyTextPanel lcdOutput = null;

        IMyShipConnector connector = null;

        List<IMyBatteryBlock> batteryBlocks = new List<IMyBatteryBlock>();

        List<IMySensorBlock> sensors = new List<IMySensorBlock>();

        List<IMyCargoContainer> cargoContainers = new List<IMyCargoContainer>();

        List<IMyOreDetector> detectors = new List<IMyOreDetector>();

        List<IMyRadioAntenna> radioAntennas = new List<IMyRadioAntenna>();

        List<IMyLaserAntenna> laserAntennas = new List<IMyLaserAntenna>();

        List<IMyThrust> thrusters = new List<IMyThrust>();

        List<IMyGyro> gyros = new List<IMyGyro>();

        List<IMyShipDrill> drills = new List<IMyShipDrill>();

        List<IMyShipGrinder> grinders = new List<IMyShipGrinder>();

        // our tag BOT

        string tag = "(BASE)";

        MyDetectedEntityInfo? lastDetectedEntity = null;

        List<WatchedObject> watchedObjects = new List<WatchedObject>();

        public MyBot()
        {
            Setup();
        }

        public void Setup()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update100; // Run Each tick, no timerblock needed

            List<IMyTerminalBlock> taggedBlockes = FindBlocksWithTag();

            foreach (var block in taggedBlockes)
            {
                string typeName = block.GetType().Name;

                switch (typeName)
                {
                    case "MyRemoteControl":
                        remoteControl = block as IMyRemoteControl;
                        break;
                    case "MyTextPanel":
                        {
                            Echo.Invoke(string.Format("Assigned {0} to lcd", block.DisplayNameText));
                            lcdOutput = block as IMyTextPanel;
                        }
                        break;
                    case "MyBatteryBlock":
                        {
                            if (batteryBlocks.Find(b => b.Name == block.Name) == null)
                                batteryBlocks.Add(block as IMyBatteryBlock);
                        }
                        break;
                    case "MySensorBlock":
                        {
                            if (sensors.Find(b => b.Name == block.Name) == null)
                                sensors.Add(block as IMySensorBlock);
                        }
                        break;

                    case "MyCargoContainer":
                        {
                            if (cargoContainers.Find(b => b.Name == block.Name) == null)
                                cargoContainers.Add(block as IMyCargoContainer);
                        }
                        break;
                    case "MyOreDetector":
                        {
                            if (detectors.Find(b => b.Name == block.Name) == null)
                                detectors.Add(block as IMyOreDetector);
                        }
                        break;
                    case "MyThrust":
                        {
                            if (thrusters.Find(b => b.Name == block.Name) == null)
                                thrusters.Add(block as IMyThrust);
                        }
                        break;
                    case "MyShipConnector":
                        {
                            connector = block as IMyShipConnector;
                        }
                        break;
                    case "MyRadioAntenna":
                        {
                            if (radioAntennas.Find(b => b.Name == block.Name) == null)
                                radioAntennas.Add(block as IMyRadioAntenna);
                        }
                        break;
                    case "MyLaserAntenna":
                        {
                            if (laserAntennas.Find(b => b.Name == block.Name) == null)
                                laserAntennas.Add(block as IMyLaserAntenna);
                        }
                        break;
                    case "MyGyro":
                        {
                            if (gyros.Find(b => b.Name == block.Name) == null)
                                gyros.Add(block as IMyGyro);
                        }
                        break;
                    case "MyShipDrill":
                        {
                            if (drills.Find(b => b.Name == block.Name) == null)
                                drills.Add(block as IMyShipDrill);
                        }
                        break;
                    case "MyShipGrinder":
                        {
                            if (grinders.Find(b => b.Name == block.Name) == null)
                                grinders.Add(block as IMyShipGrinder);
                        }
                        break;
                    default:
                        Echo.Invoke(string.Format("No Match to {0}", typeName));
                        break;
                }


            }
        }
        public void Main(string argument, UpdateType updateSource)
        {
            string[] args = argument.ToLower().Split(' ');

            // switch (args[0])
            //  { 

            //  }

            Update();
        }

        public void Update()
        {
            UseSensors();

            UpdateDisplay();
        }
        public void RemoteControl()
        {
            if (remoteControl != null)
            {

            }
        }

        public void UseSensors()
        {
            if (sensors.Count > 0)
            {
                for (int i = 0; i < sensors.Count; i++)
                {
                    if (!lastDetectedEntity.HasValue)
                    {
                        if (lastDetectedEntity.Value.EntityId != sensors[i].EntityId)
                        {
                            lastDetectedEntity = sensors[i].LastDetectedEntity;

                            if (lcdOutput != null)
                                lcdOutput.WriteText(string.Format("Hi {0}", lastDetectedEntity.Value.Name));
                        }
                    }
                }
            }
        }

        void UpdateDisplay()
        {
            if (lcdOutput != null)
            {
                StringBuilder b = new StringBuilder();

                List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();

                GridTerminalSystem.GetBlocks(blocks);

                watchedObjects.Clear();

                foreach(var block in blocks)
                {
                    if(block.HasInventory)
                    {
                        List<MyInventoryItem> items = new List<MyInventoryItem>();

                        var inventory = block.GetInventory();

                        inventory.GetItems(items);

                        foreach(var item in items)
                        {
                            WatchedObject wobj = new WatchedObject(item.Type, item.Amount);

                            bool updated = false;

                            for(int i = 0;i<watchedObjects.Count;i++)
                            {
                                if (watchedObjects[i].Type == wobj.Type)
                                {
                                    watchedObjects[i].Amount += wobj.Amount;

                                    updated = true;
                                }
                            }

                            if (!updated)
                                watchedObjects.Add(wobj);
                        }
                    }
                }

                List<WatchedObject> ores = new List<WatchedObject>();

                List<WatchedObject> Ingots = new List<WatchedObject>();

                List<WatchedObject> Components = new List<WatchedObject>();

                foreach (var item in watchedObjects)
                {
                  //  if (item.Type.ToString().Contains("Component"))
                      //  Components.Add(item);
                    if (item.Type.ToString().Contains("Ore"))
                        ores.Add(item);
                    if (item.Type.ToString().Contains("Ingot"))
                        Ingots.Add(item);

                   // if(item.Type.ToString().Contains("Ingot")
                        //|| item.Type.ToString().Contains("Ore"))
                   //.AppendLine(string.Format("{0} : {1}", item.Type.ToString().Split("MyObjectBuilder_").Last(), item.Amount));
                }



                for(int i = 0;i<ores.Count;i++)
                {
                    string ore = string.Format("{0} : {1}", ores[i].Type.ToString().Split('/').Last(), ores[i].Amount);
                    
                    for(int j = 0;j>Ingots.Count;j++)
                    {
                        ore += string.Format("  {0} : {1}", Ingots[j].Type.ToString().Split('/').Last(), Ingots[j].Amount);
                    }

                    b.AppendLine(ore);
                }

                lcdOutput.WriteText(b.ToString());
            }
            else
            {
                Echo.Invoke("Unable in init lcd !!");
            }
        }
        #region Helper Code

        public List<IMyTerminalBlock> FindBlocksWithTag()
        {
            List<IMyTerminalBlock> result = new List<IMyTerminalBlock>();

            List<IMyTerminalBlock> temp = new List<IMyTerminalBlock>();

            GridTerminalSystem.GetBlocks(temp);

            foreach (var block in temp)
            {
                if (block.DisplayNameText.EndsWith(tag))
                    result.Add(block);
            }

            return result;
        }
        /// <summary>
        /// Finds a group of blocks by name and type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<T> FindBlockGroup<T>(string name) where T : class
        {
            List<T> result = new List<T>();

            var group = GridTerminalSystem.GetBlockGroupWithName(name);

            group.GetBlocksOfType(result);

            return result;
        }

        public class WatchedObject
        {
            public MyItemType Type { get; set; }

            public MyFixedPoint Amount { get; set; }

            public WatchedObject(MyItemType type, MyFixedPoint amt)
            {
                this.Type = type;

                this.Amount = amt;
            }
        }
        #endregion

        #endregion
    }
}
