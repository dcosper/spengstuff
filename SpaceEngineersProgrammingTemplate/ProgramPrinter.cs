﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game;
using VRage.Game.GUI.TextPanel;
using VRageMath;

namespace SpaceEngineersProgrammingTemplate
{
    public class ProgramPrinter : MyGridProgram
    {
        #region Code
        // set up our groups
        List<IMyPistonBase> LeftPrinterPistonGroup = new List<IMyPistonBase>();

        List<IMyPistonBase> RightPrinterPistonGroup = new List<IMyPistonBase>();

        List<IMyShipWelder> LeftWelderGroup = new List<IMyShipWelder>();

        List<IMyShipWelder> RightWelderGroup = new List<IMyShipWelder>();

        float velocity = 0.3f;

        bool isPrinting = false;

        bool isReadyToPrint = false;

        bool leftDone = false;

        bool rightDone = false;

        public ProgramPrinter()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set RuntimeInfo.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.
            LeftPrinterPistonGroup = FindBlockGroup<IMyPistonBase>("Piston Group L (LARGE PRINTER)");

            RightPrinterPistonGroup = FindBlockGroup<IMyPistonBase>("Piston Group R (LARGE PRINTER)");

            LeftWelderGroup = FindBlockGroup<IMyShipWelder>("Welder Group L Side (LARGE PRINTER)");

            RightWelderGroup = FindBlockGroup<IMyShipWelder>("Welder Group R Side (LARGE PRINTER)");

            StartScript();
        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from.
            // 
            // The method itself is required, but the arguments above
            // can be removed if not needed.

            string[] args = argument.ToLower().Split(' ');

            switch(args[0])
            {
                case "turnoff":
                    TurnOff();
                    break;
                case "turnon":
                    TurnWeldersOn();
                    break;
                case "print":
                    Print();
                    break;
                case "reset":
                    {
                        TurnOff();

                        StopScript();
                    }
                    break;
            }

            Update();
        }
        public void Print()
        {
            isReadyToPrint = true;
            // move to 4.5f
            // set min distance to max distance

            StartScript();

            // extend

            foreach (var piston in LeftPrinterPistonGroup)
            {
                piston.Velocity = velocity;

                piston.Extend();
            }

            foreach (var piston in RightPrinterPistonGroup)
            {
                piston.Velocity = velocity;

                piston.Extend();
            }
        }
        public void Setup()
        {
            // the welders need to be off

            TurnOff();
        }
        public void TurnOff()
        {
            isPrinting = false;

            isReadyToPrint = false;

            leftDone = false;

            rightDone = false;

            foreach(var welder in LeftWelderGroup)
                welder.ApplyAction("OnOff_Off");

            foreach (var piston in LeftPrinterPistonGroup)
            {
                piston.Velocity = velocity;

                piston.Retract();
            }

            foreach (var piston in RightPrinterPistonGroup)
            {
                piston.Velocity = velocity;

                piston.Retract();
            }
        }
        public void TurnWeldersOn()
        {
            foreach (var welder in LeftWelderGroup)
                welder.ApplyAction("OnOff_On");

            foreach (var welder in RightWelderGroup)
                welder.ApplyAction("OnOff_On");
        }
        public void Update()
        {
            if (isReadyToPrint)
            {
                int totalReadyToPrint = 0;

                foreach (var piston in LeftPrinterPistonGroup)
                {
                    if (piston.CurrentPosition == piston.MaxLimit)
                        totalReadyToPrint++;
                }

                foreach (var piston in LeftPrinterPistonGroup)
                {
                    if (piston.CurrentPosition == piston.MaxLimit)
                        totalReadyToPrint++;
                }

                if(totalReadyToPrint == LeftPrinterPistonGroup.Count + RightPrinterPistonGroup.Count)
                {
                    // turn on welder

                    TurnWeldersOn();

                    isPrinting = true;

                    isReadyToPrint = false;

                    foreach (var piston in LeftPrinterPistonGroup)
                    {
                        piston.Retract();
                    }

                    foreach (var piston in RightPrinterPistonGroup)
                    {
                        piston.Retract();
                    }
                }
            }

            if(isPrinting)
            {
                int totalComplete = 0;

                foreach (var piston in LeftPrinterPistonGroup)
                {
                    if (piston.CurrentPosition == piston.MinLimit)
                        totalComplete++;
                }

                foreach (var piston in LeftPrinterPistonGroup)
                {
                    if (piston.CurrentPosition == piston.MinLimit)
                        totalComplete++;
                }


                if(totalComplete == LeftPrinterPistonGroup.Count + RightPrinterPistonGroup.Count)
                {
                    isPrinting = false;

                    TurnOff();
                }
            }
        }
        public void StartScript()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update10;
        }
        public void StopScript()
        {
            isPrinting = false;

            isReadyToPrint = false;

            Runtime.UpdateFrequency = UpdateFrequency.None;
        }
        /// <summary>
        /// Finds a group of blocks by name and type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<T> FindBlockGroup<T>(string name) where T : class
        {
            List<T> result = new List<T>();

            var group = GridTerminalSystem.GetBlockGroupWithName(name);

            group.GetBlocksOfType(result);

            return result;
        }

        #endregion
    }
}
