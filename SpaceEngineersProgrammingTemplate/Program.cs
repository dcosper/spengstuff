﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using VRage;
using VRage.Game.ModAPI.Ingame;

namespace SpaceEngineersProgrammingTemplate
{
    public class Program : MyGridProgram
    {
        #region CODE
        // tag to look for lcd

        // First rotor rght side, close to base
        string _rotorTag01 = "Rotor Left Alpha";

        // Second rotor right side Far from base
        string _rotorTag02 = "Rotor Left Beta";

        // Third rotor Left side, close to base
        string _rotorTag03 = "Rotor Right Alpha";

        // 4th rotor left side, far fom base
        string _rotorTag04 = "Rotor Right Beta";

        List<IMyMotorStator> Rotors;

        float rotationVelocity = 3f;

        IMyMotorStator _leftAlpha;

        IMyMotorStator _leftBeta;

        IMyMotorStator _rightAlpha;

        IMyMotorStator _rightBeta;

        RotorControl _leftAlphaControl;

        RotorControl _leftBetaControl;

        RotorControl _rightAlphaControl;

        RotorControl _rightBetaControl;

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update10; // Run Each 10 ticks

            _leftAlpha = GridTerminalSystem.GetBlockWithName(_rotorTag01) as IMyMotorStator;

            _leftBeta = GridTerminalSystem.GetBlockWithName(_rotorTag02) as IMyMotorStator;

            _rightAlpha = GridTerminalSystem.GetBlockWithName(_rotorTag03) as IMyMotorStator;

            _rightBeta = GridTerminalSystem.GetBlockWithName(_rotorTag04) as IMyMotorStator;

            _leftAlphaControl = new RotorControl(_leftAlpha, 0f, -90f);

            _leftBetaControl = new RotorControl(_leftBeta, 360f, 180f);

            _rightAlphaControl = new RotorControl(_rightAlpha, 0f, 90f);

            _rightBetaControl = new RotorControl(_rightBeta, -360f, 180f);
        }
        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            string[] args = argument.ToLower().Split(' ');

            switch (args[0])
            {
                case "deploy":
                    {
                        _leftAlphaControl.MoveToDeployedState();

                        _leftBetaControl.MoveToDeployedState();

                        _rightAlphaControl.MoveToDeployedState();

                        _rightBetaControl.MoveToDeployedState();
                    }
                    break;
                case "retract":
                    {
                        _leftAlphaControl.MoveToRestingState();

                        _leftBetaControl.MoveToRestingState();

                        _rightAlphaControl.MoveToRestingState();

                        _rightBetaControl.MoveToRestingState();
                    }
                    break;
                default:
                    break;
            }
            // update rotors here ?

            _leftAlphaControl.Update();

            _leftBetaControl.Update();
        }
        public class RotorControl
        {
            IMyMotorStator _rotor;

            float _restingAngle = 0f;

            float _deployedAngle = 180f;

            public float RotorSpeed = 3f;

            public bool AngleReached = false;

            public RotorControl(IMyMotorStator rotor, float restingAngle, float deployedAngle)
            {
                _rotor = rotor;

                _restingAngle = restingAngle;

                _deployedAngle = deployedAngle;
            }

            public void Update()
            {
                if (_rotor.Angle == _restingAngle ||
                    _rotor.Angle == _deployedAngle)
                    AngleReached = true;
            }

            public void MoveToRestingState(float speed = 0)
            {
                if (_rotor.Angle != _restingAngle)
                    AngleReached = false;

                SetRotorAngle(_rotor, _restingAngle, speed);
            }
            public void MoveToDeployedState(float speed = 0)
            {
                if (_rotor.Angle != _deployedAngle)
                    AngleReached = false;

                SetRotorAngle(_rotor, _deployedAngle, speed);
            }
            void SetRotorAngle(IMyMotorStator rotor, float angle, float speed = 0)
            {
                float currentAngle = rotor.Angle / (float)Math.PI * 180f;

                float rotationSpeed = speed;

                if (speed == 0)
                    rotationSpeed = RotorSpeed;

                if (angle > currentAngle)
                {
                    rotor.SetValue<float>("UpperLimit", angle);
                    rotor.SetValue<float>("LowerLimit", -361f);
                    rotor.SetValue<float>("Velocity", rotationSpeed);
                }
                else
                {
                    rotor.SetValue<float>("LowerLimit", angle);
                    rotor.SetValue<float>("UpperLimit", 361f);
                    rotor.SetValue<float>("Velocity", -rotationSpeed);
                }
            }
        }
        #endregion
    }
}
