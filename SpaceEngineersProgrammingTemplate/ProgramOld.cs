﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game;
using VRage.Game.GUI.TextPanel;
using VRageMath;

namespace SpaceEngineersProgrammingTemplate
{
    public class ProgramOld : MyGridProgram
    {
        public UpdateType LastUpdateType = UpdateType.None;

        List<IMyShipDrill> Drills = new List<IMyShipDrill>();

        // groups of 5
        List<IMyPistonBase> VerticalUPPistons = new List<IMyPistonBase>();

        List<IMyPistonBase> VerticalDownPistons1 = new List<IMyPistonBase>();

        List<IMyPistonBase> VerticalDownPistons2 = new List<IMyPistonBase>();

        IMyTextPanel outText;

        float pistonSpeed = -1f;

        DrillState CurrentDrillState = DrillState.None;

        
        public ProgramOld()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set RuntimeInfo.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.

            Echo = EchoOut; // for echo redirect

            Runtime.UpdateFrequency = UpdateFrequency.Update1; // Run Each tick, no timerblock needed

            // Find the groups

            outText = GridTerminalSystem.GetBlockWithName("Mobile Drill LCD panel") as IMyTextPanel;

 
        }
        public void Main(string arg, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from.
            // 
            // The method itself is required, but the arguments above
            // can be removed if not needed.

            LastUpdateType = updateSource;

            switch(arg.ToLower())
            {
                case "stop":
                    Stop();
                    break;
                case "on":
                    TurnOn();
                    break;
                case "off":
                    TurnOff();
                    break;
                case "reset":
                    Reset();
                    break;
            }

            // run our logic loop

            Update(updateSource);
        }

        public bool AreGroupsActive
        {
            get
            {
                if (outText == null)
                    return false;

                if (VerticalUPPistons.Count == 0)
                    return false;

                if (VerticalDownPistons1.Count == 0)
                    return false;

                if(VerticalDownPistons2.Count == 0)
                    return false;

                if (Drills.Count == 0)
                    return false;

                outText.ContentType = ContentType.TEXT_AND_IMAGE;

                outText.BackgroundColor = Color.Black;

                outText.Alignment = TextAlignment.CENTER;

                return true;
            }
        }
        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }
        public void Stop()
        {
            // Stops this script from running after this update
            Runtime.UpdateFrequency = UpdateFrequency.None;

            return;
        }
        /// <summary>
        /// Appends text to current programable blocks surface
        /// remove this if using a lcd
        /// </summary>
        /// <param name="text"></param>
        public void EchoOut(string text)
        {
            if (((IMyTextSurfaceProvider)Me).SurfaceCount > 0)
            {
                string extra = "";
                if (text.Length != 0) extra = Environment.NewLine;
                IMyTextSurface surface = ((IMyTextSurfaceProvider)Me).GetSurface(0);
                surface.ContentType = ContentType.TEXT_AND_IMAGE;
                surface.WriteText(extra + text, true);
            }
        }
        public void Update(UpdateType updateSource)
        {
            var drillsGroup = GridTerminalSystem.GetBlockGroupWithName("Mobile Drills");

            drillsGroup.GetBlocksOfType(Drills);

            var upPistonGroup = GridTerminalSystem.GetBlockGroupWithName("Mobile Pistons (UP)");

            upPistonGroup.GetBlocksOfType(VerticalUPPistons);

            var downPistonGroup1 = GridTerminalSystem.GetBlockGroupWithName("Mobile Pistons 1 (Down)");

            downPistonGroup1.GetBlocksOfType(VerticalDownPistons1);

            var downPistonGroup2 = GridTerminalSystem.GetBlockGroupWithName("Mobile Pistons 2 (Down)");

            downPistonGroup2.GetBlocksOfType(VerticalDownPistons2);

            foreach (var piston in VerticalDownPistons1)
                piston.Velocity = pistonSpeed;

            foreach (var piston in VerticalDownPistons2)
                piston.Velocity = pistonSpeed + 1f;

            foreach (var piston in VerticalUPPistons)
                piston.Velocity = pistonSpeed;

            bool upComplete = false;

            bool downComplete = false;
            // we are drilling here

            // Are the up pistons fully retracted ?

            float currentPosUp = 0.0f;

            foreach (var piston in VerticalUPPistons)
                currentPosUp += piston.CurrentPosition;
            // we should be zero if we are finished retracting

            if (currentPosUp > 0.0f)
                upComplete = true;

            // are the down pistions fully extended ?
            
            float currentPosDown = 0.0f;

            foreach (var piston in VerticalDownPistons1)
                currentPosDown += piston.CurrentPosition;

            foreach (var piston in VerticalDownPistons2)
                currentPosDown += piston.CurrentPosition;

            if (currentPosDown == (VerticalDownPistons1.Count + VerticalDownPistons2.Count) * 10)
                downComplete = true;

            List<PistonStatus> upPistonStatus = GetGroupPistonStatus(VerticalUPPistons);

            List<PistonStatus> down1PistonStatus = GetGroupPistonStatus(VerticalDownPistons1);

            List<PistonStatus> down2PistonStatus = GetGroupPistonStatus(VerticalDownPistons2);

            // ok now activate the drill
            bool upPistonsMoving = false;

            bool down1PistonMoving = false;

            bool down2PistonMoving = false;

            if (down1PistonStatus.Contains(PistonStatus.Extending)
                || down1PistonStatus.Contains(PistonStatus.Retracting))
                down1PistonMoving = true;

            if (down2PistonStatus.Contains(PistonStatus.Extending)
                || down2PistonStatus.Contains(PistonStatus.Retracting))
                down2PistonMoving = true;

            if (upPistonStatus.Contains(PistonStatus.Extending)
                || upPistonStatus.Contains(PistonStatus.Retracting))
                upPistonsMoving = true;

            if (!down1PistonMoving && !down2PistonMoving && !upPistonsMoving)
            {
                // we are completly stopped, if up pistons are fully retracted
                // and all down pistons are fully extended, we are done
                bool completed = true;

                foreach (var piston in VerticalDownPistons1)
                {
                    if (piston.Status != PistonStatus.Extended)
                        completed = false;
                }

                foreach (var piston in VerticalDownPistons2)
                {
                    if (piston.Status != PistonStatus.Extended)
                        completed = false;
                }

                foreach (var piston in VerticalUPPistons)
                {
                    if (piston.Status != PistonStatus.Retracted)
                        completed = false;
                }

                if (completed)
                    CurrentDrillState = DrillState.Completed;
                else
                {
                    //start us up
                    ExtendPistonGroup(VerticalDownPistons1);

                    down1PistonMoving = true;

                    down2PistonMoving = false;

                    upPistonsMoving = false;

                    CurrentDrillState = DrillState.Drilling;
                }
            }
            // ok if none were moving, the above block ran and started the first block going down, lets see is it is moving now


            if(CurrentDrillState == DrillState.Drilling)
            {
                // is the first section done ?
                if (!down1PistonMoving && !upPistonsMoving)
                {
                    ExtendPistonGroup(VerticalDownPistons2);

                    down1PistonMoving = false;

                    down2PistonMoving = true;

                    upPistonsMoving = false;
                }

                else if(!down1PistonMoving && !down2PistonMoving)
                {
                    RetractPistonGroup(VerticalUPPistons);

                    down1PistonMoving = false;

                    down2PistonMoving = false;

                    upPistonsMoving = true;
                }
            }

            StringBuilder info = new StringBuilder();

            info.AppendLine(string.Format("Drill State {0}", CurrentDrillState));

            info.AppendLine(string.Format("Up Pistons Position {0} Total Pistons {1}",
                GetPistonGroupPosition(VerticalUPPistons), VerticalUPPistons.Count));

            for (int i = 0; i < upPistonStatus.Count; i++)
                info.AppendLine(string.Format("Piston {0} Status: {1}", i, upPistonStatus[i]));

            info.AppendLine(string.Format("Down Pistons Position {0} Total Pistons {1}",
                GetPistonGroupPosition(VerticalDownPistons1) + GetPistonGroupPosition(VerticalDownPistons2)
                , VerticalDownPistons1.Count + VerticalDownPistons2.Count));

            for (int i = 0; i < down1PistonStatus.Count; i++)
                info.AppendLine(string.Format("Piston {0} Status: {1}", i, down1PistonStatus[i]));
            for (int i = 0; i < down2PistonStatus.Count; i++)
                info.AppendLine(string.Format("Piston {0} Status: {1}", i, down2PistonStatus[i]));

            WriteToLCD(info.ToString());

            if (CurrentDrillState == DrillState.Completed)
            {
                Reset();
            }
        }
        
        #region Helper Functions
        public void WriteToLCD(string value)
        {
            outText.WriteText(value+Environment.NewLine);
        }
        public void TurnOn()
        {
            foreach(var drill in Drills)
                drill.ApplyAction("OnOff_On");

            foreach(var upPiston in VerticalUPPistons)
                upPiston.ApplyAction("OnOff_On");

            foreach(var downPiston in VerticalDownPistons1)
                downPiston.ApplyAction("OnOff_On");

            foreach (var downPiston in VerticalDownPistons2)
                downPiston.ApplyAction("OnOff_On");
        }
        public void TurnOff()
        {
            foreach (var drill in Drills)
                drill.ApplyAction("OnOff_Off");

            foreach (var upPiston in VerticalUPPistons)
                upPiston.ApplyAction("OnOff_Off");

            foreach (var downPiston in VerticalDownPistons1)
                downPiston.ApplyAction("OnOff_Off");

            foreach (var downPiston in VerticalDownPistons2)
                downPiston.ApplyAction("OnOff_Off");
        }
        public void Reset()
        {
            // the vertical up needs to be fully extended (10)
            // while vertical down fully collapsed (0)
            ExtendPistonGroup(VerticalUPPistons);

            RetractPistonGroup(VerticalDownPistons1);

            RetractPistonGroup(VerticalDownPistons2);

            CurrentDrillState = DrillState.Resetting;

            Stop();
        }

        public enum DrillState
        {
            None,
            Completed,
            Drilling,
            Resetting,
            Error
        }

        public float GetPistonGroupPosition(List<IMyPistonBase> pistons)
        {
            float result = 0.0f;

            foreach (var piston in pistons)
                result += piston.CurrentPosition;

            return result;
        }

        public List<PistonStatus> GetGroupPistonStatus(List<IMyPistonBase> pistons)
        {
            List<PistonStatus> result = new List<PistonStatus>();

            foreach (var piston in pistons)
                result.Add(piston.Status);

            return result;
        }
        public void ExtendPistonGroup(List<IMyPistonBase> pistons)
        {
            foreach (var piston in pistons)
                piston.Extend();
        }
        public void RetractPistonGroup(List<IMyPistonBase> pistons)
        {
            foreach (var piston in pistons)
                piston.Retract();
        }
        #endregion
    }
}
