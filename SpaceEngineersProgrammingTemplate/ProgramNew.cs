﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game;
using VRage.Game.GUI.TextPanel;
using VRageMath;

namespace SpaceEngineersProgrammingTemplate
{
    public class ProgramNew : MyGridProgram
    {
        #region Code
        IMyTextPanel lcd;

        IMyMotorStator baseRotor;

        public ProgramNew()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set RuntimeInfo.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.

            Runtime.UpdateFrequency = UpdateFrequency.Update10;

            // find our LCD
            lcd = GridTerminalSystem.GetBlockWithName("Mobile Connector LCD") as IMyTextPanel;


            baseRotor = GridTerminalSystem.GetBlockWithName("Grid Connector Rotor") as IMyMotorStator;

            WriteIdleMessage();

            SetRotorAngles("Grid Connector Rotor", 180f, -30f);

            List<ITerminalAction> actions = new List<ITerminalAction>();

            baseRotor.GetActions(actions);

            WriteLine("", false);

            foreach (var act in actions)
                WriteLine(string.Format("Actioin: {0}", act.Name));
        }
        public void WriteIdleMessage()
        {
            WriteLine("Conector Registered");
        }
        public void Write(string value, bool append = true)
        {
            lcd.WriteText(value, append);
        }
        public void WriteLine(string value, bool append = true)
        {
            lcd.WriteText(value + Environment.NewLine, append);
        }
        void SetRotorAngles(string rotorName, float angle, float rotationVelocity)
        {
            var rotors = new List<IMyTerminalBlock>();

            GridTerminalSystem.SearchBlocksOfName(rotorName, rotors);

            foreach (IMyMotorStator thisRotor in rotors)
            {
                float currentAngle = thisRotor.Angle / (float)Math.PI * 180f;

                Echo("Current Angle: " + currentAngle.ToString());
                Echo("New Angle: " + angle.ToString());

                if (angle > currentAngle)
                {
                    thisRotor.SetValue<float>("UpperLimit", angle);
                    thisRotor.SetValue<float>("LowerLimit", 0);
                    thisRotor.SetValue<float>("Velocity", -rotationVelocity);
                }
                else
                {
                    thisRotor.SetValue<float>("LowerLimit", 0);
                    thisRotor.SetValue<float>("UpperLimit", angle);
                    thisRotor.SetValue<float>("Velocity", rotationVelocity);
                }
            }
        }
        public void Stop()
        {
            // Stops this script from running after this update
            Runtime.UpdateFrequency = UpdateFrequency.None;

            return;
        }
        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from.
            // 
            // The method itself is required, but the arguments above
            // can be removed if not needed.

            string[] args = argument.ToLower().Split(' ');

            switch(args[0])
            {
                case "rotate":
                    {
                        float angle = 0.0f;

                        if(args.Length >=2)
                        {
                            if (float.TryParse(args[1], out angle))
                                SetRotorAngles("Grid Connector Rotor", angle, -30f);
                        }
                    }
                    break;
            }

        }

        #endregion
    }
}
