﻿using VRage.Game.ModAPI.Ingame;

namespace SpaceEngineersProgrammingTemplate
{
    internal interface IMyTimerBlock
    {
        void Trigger();
        string CustomName { get; set; }

        IMyCubeGrid CubeGrid { get; set; }
    }
}