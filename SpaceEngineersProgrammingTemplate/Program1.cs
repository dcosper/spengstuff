﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.ModAPI.Ingame;
using VRage;
using VRage.Game.ModAPI.Ingame;

namespace SpaceEngineersProgrammingTemplate
{
    public class Program1 : MyGridProgram
    {
       #region CODE
        // tag to look for lcd

        string _tag = "[LCD]";

        PageManager _pageManager = null;

        public Program1()
        {
            _pageManager = new PageManager();

            Page page1 = new Page();

            page1.AddLine("Welcome to LCDOutput", new Action(()=>{ }), false);

            page1.AddLine("____________________", new Action(() => { }), false);

            page1.AddLine("One", new Action(() => { }));

            page1.AddLine("Two", new Action(() => { }));

            page1.AddLine("Three", new Action(() => { }));

            page1.AddLine("Four", new Action(() => { }));

            page1.AddLine("Five", new Action(() => { }));

            _pageManager.AddPage(page1);

            _pageManager.RedrawScreen();
        }

        public void Main(string argument, UpdateType updateSource)
        {
            string[] args = argument.ToLower().Split(' ');

            switch (args[0])
            {
                case "up":
                    _pageManager.RedrawScreen();
                    break;
                case "down":
                    _pageManager.RedrawScreen();
                    break;
                case "apply":
                    _pageManager.RedrawScreen();
                    break;
                default:
                    break;
            }
        }

        public List<IMyTerminalBlock> FindBlocksWithTag()
        {
            List<IMyTerminalBlock> result = new List<IMyTerminalBlock>();

            List<IMyTerminalBlock> temp = new List<IMyTerminalBlock>();

            GridTerminalSystem.GetBlocks(temp);

            foreach (var block in temp)
            {
                if (block.DisplayNameText.Contains(_tag))
                    result.Add(block);
            }

            return result;
        }
        // Helper classes

        public class PageManager : MyGridProgram
        {
            public List<Page> Pages { get; set; }

            private int _currentIndex = 0;

            IMyTextSurface _lcd;

            public PageManager()
            {
                Pages = new List<Page>();
            }

            public void SetLCD(IMyTextSurface lcdOutput)
            {
                _lcd = lcdOutput;
            }

            public Page DisplayNextPage()
            {
                if (Pages == null)
                    return null;
                if (Pages.Count == 0)
                    return null;
                if (_currentIndex + 1 > Pages.Count)
                    _currentIndex = 0;

                _currentIndex++;

                Page nextPage = Pages[_currentIndex];

                WriteDataOutput(nextPage);

                return nextPage;
            }

            public Page GetPreviousPage()
            {
                if (Pages == null)
                    return null;
                if (Pages.Count == 0)
                    return null;
                if (_currentIndex - 1 < 0)
                    _currentIndex = Pages.Count -1;

                _currentIndex--;

                Page nextPage = Pages[_currentIndex];

                WriteDataOutput(nextPage);

                return nextPage;
            }

            public void AddPage(Page page)
            {
                Pages.Add(page);
            }

            public void RedrawScreen()
            {
                if(Pages[_currentIndex] != null)
                    WriteDataOutput(Pages[_currentIndex]);
            }

            public void WriteDataOutput(Page page)
            {
                if (page == null)
                    return;

                if(_lcd != null)
                    _lcd.WriteText(page.ToString());

                Echo.Invoke(page.ToString());
            }
        }

        public class Page
        {
            // each page can have 12 lines only of 24 chars per line

            public List<LCDLine> Lines = new List<LCDLine>();

            private int _selectedLineIndex = 0;

            public LCDLine Selected { get; set; }

            public delegate void SelectedLineChange(int index);

            public event SelectedLineChange SelectedLineChangedEvent;

            public void AddLine(string data, Action lineAction, bool canSelect = true)
            {
                if (Lines.Count == 12)
                    return;

                Lines.Add(new LCDLine(data, lineAction, canSelect));
            }

            public void SelectNextLine()
            {
                if (_selectedLineIndex + 1 > Lines.Count)
                    _selectedLineIndex = 0;

                _selectedLineIndex++;

                Selected = Lines[_selectedLineIndex];

                SelectedLineChangedEvent?.Invoke(_selectedLineIndex);
            }

            public void SelectPreviousLine()
            {
                if (_selectedLineIndex - 1 < 0)
                    _selectedLineIndex = Lines.Count -1;

                _selectedLineIndex--;

                Selected = Lines[_selectedLineIndex];

                SelectedLineChangedEvent?.Invoke(_selectedLineIndex);
            }

            public void Apply()
            {
                Lines[_selectedLineIndex].PerformLineAction();
            }

            public override string ToString()
            {
                StringBuilder result = new StringBuilder();

                for(int i = 0;i<Lines.Count;i++)
                {
                    if (Lines[i] == null)
                        continue;

                    if (_selectedLineIndex == i && Lines[i].CanSelect)
                        result.AppendLine(">" + Lines[i].ToString());
                    else result.AppendLine(Lines[i].ToString());
                }

                return result.ToString();
            }
        }

        public class LCDLine
        {
            public List<char> Line = new List<char>();

            private Action _lineAction;

            public bool CanSelect = true;

            public LCDLine(string data, Action action, bool canSelect = true)
            {
                _lineAction = action;

                CanSelect = canSelect;

                ConvertString(data);
            }

            public override string ToString()
            {
                return new string(Line.ToArray());
            }

            public void PerformLineAction()
            {
                _lineAction?.Invoke();
            }

            /// <summary>
            /// Converts the string into a char array and enforces the data length
            /// will trunicate any string longer than allowed
            /// </summary>
            /// <param name="data"></param>
            void ConvertString(string data)
            {
                Line.Clear();

                char[] c = data.ToCharArray();

                for (int i = 0; i < c.Length; i++)
                {
                    if(i <= 24)
                        Line.Add(c[i]);
                }
            }
        }
        #endregion 
    }
}
