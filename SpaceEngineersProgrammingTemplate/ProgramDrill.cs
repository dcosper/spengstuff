﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game;
using VRage.Game.GUI.TextPanel;
using VRageMath;

namespace SpaceEngineersProgrammingTemplate
{
    public class ProgramDrill : MyGridProgram
    {
        #region Code

        // The piston status groups for monitoring what state the pistons are in 
        PistonGroupStatus upPistonStatus;

        PistonGroupStatus downPiston1Status;

        PistonGroupStatus downPiston2Status;

        PistonGroupStatus downPiston3Status;

       // PistonGroupStatus downPiston4Status;

        /* The Pistons are broken into groups of 5
           this is done because moving over 20 pistons even at 0.01 slams the ground
           and damages the drill, so we break them into mangable groups, and then move
           the entire group at 0.01 velocity, we then monitor each group
           to completion
        */
        
        // 5 pistons that start fully extended as they go up in order to give the drill more depth
        List<IMyPistonBase> VerticalUPPistons = new List<IMyPistonBase>();

        // 5 pistons that extend downwards into the ground
        List<IMyPistonBase> VerticalDownPistons1 = new List<IMyPistonBase>();

        // 5 pistons that extend downwards into the ground
        List<IMyPistonBase> VerticalDownPistons2 = new List<IMyPistonBase>();

        // 5 pistons that extend downwards into the ground
        List<IMyPistonBase> VerticalDownPistons3 = new List<IMyPistonBase>();

        // 3 pistons that extend downwards into the ground
        List<IMyPistonBase> VerticalDownPistons4 = new List<IMyPistonBase>();

        // Our drills (the rotor (if you use one) should be included in this group, and set to rotate at the desired speed via the K menu
        List<IMyShipDrill> Drills = new List<IMyShipDrill>();

        // Our suspension of the wheeled vehicle
        List<IMyMotorSuspension> Wheels = new List<IMyMotorSuspension>();

        // Our LCD Display
        IMyTextPanel lcdOutput;

        // The extimated depth we can go
        float Depth = 0.0f;

        // If we are currently drilling or not
        bool isDrilling = false;

        public ProgramDrill()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script. 
            //     
            // The constructor is optional and can be removed if not
            // needed.
            // 
            // It's recommended to set RuntimeInfo.UpdateFrequency 
            // here, which will allow your script to run itself without a 
            // timer block.
            Runtime.UpdateFrequency = UpdateFrequency.Update1; // Run Each tick, no timerblock needed

            // find our LCD
            lcdOutput = GridTerminalSystem.GetBlockWithName("B-01 LCD panel") as IMyTextPanel;

            // Find our first group of pistons

            VerticalUPPistons = FindBlockGroup<IMyPistonBase>("B-01 Piston Group (UP)");

            // Now assign the list to our monitoring class
            upPistonStatus = new PistonGroupStatus("UpPistons", VerticalUPPistons, lcdOutput);

            // subscribe to the events so we can react
            upPistonStatus.OnPistonEvent += UpPistonStatus_OnPistonEvent;

            // Find our second group of pistons

            VerticalDownPistons1 = FindBlockGroup<IMyPistonBase>("B-01 Piston Group 1 (DOWN)");

            // Now assign the list to our monitoring class
            downPiston1Status = new PistonGroupStatus("DownPistons Group 1", VerticalDownPistons1, lcdOutput);

            // subscribe to the events so we can react
            downPiston1Status.OnPistonEvent += DownPiston1Status_OnPistonEvent;

            // Find our third group of pistons

            VerticalDownPistons2 = FindBlockGroup<IMyPistonBase>("B-01 Piston Group 2 (DOWN)");

            // Now assign the list to our monitoring class
            downPiston2Status = new PistonGroupStatus("DownPistons Group 2", VerticalDownPistons2, lcdOutput);

            // subscribe to the events so we can react
            downPiston2Status.OnPistonEvent += DownPiston2Status_OnPistonEvent;

            // Find our fourth group of pistons

            VerticalDownPistons3 = FindBlockGroup<IMyPistonBase>("B-01 Piston Group 3 (DOWN)");

            // Now assign the list to our monitoring class
            downPiston3Status = new PistonGroupStatus("DownPistons Group 3", VerticalDownPistons3, lcdOutput);

            // subscribe to the events so we can react
            downPiston3Status.OnPistonEvent += DownPiston3Status_OnPistonEvent;

            // Find our fifth group of pistons

          //  VerticalDownPistons4 = FindBlockGroup<IMyPistonBase>("A-01 Piston Group 4 (DOWN)");

            // Now assign the list to our monitoring class
         //   downPiston4Status = new PistonGroupStatus("DownPistons Group 4", VerticalDownPistons4, lcdOutput);

            // subscribe to the events so we can react
          //  downPiston4Status.OnPistonEvent += DownPiston4Status_OnPistonEvent;

            // get drills

            Drills = FindBlockGroup<IMyShipDrill>("B-01 Drills");

            // Get our suspension
            Wheels = FindBlockGroup<IMyMotorSuspension>("B-01 Wheels");


            // Calculate the depth we can reach

            float depth = upPistonStatus.CalculateDepth(true);

            depth += downPiston1Status.CalculateDepth(true);

            depth += downPiston2Status.CalculateDepth(true);

            depth += downPiston3Status.CalculateDepth(true);

            //depth += downPiston4Status.CalculateDepth(true);

            depth += 2.5f * 2; // 1 for conveyor 1 for rotor

            this.Depth = depth;

            if (lcdOutput != null)
                DisplayIdleMessage();
        }

        public void DisplayIdleMessage()
        {
            WriteLine("State: Idle", false);

            WriteLine(string.Format("Estimated depth {0} meters", this.Depth));
        }
        private void DownPiston4Status_OnPistonEvent(List<IMyPistonBase> pistons, PistonGroupState state)
        {
            if (state == PistonGroupState.Extended)
            {
                Retract(); // drilling complete
            }
            else if (state == PistonGroupState.Retracted)
            {
            }
        }

        private void DownPiston3Status_OnPistonEvent(List<IMyPistonBase> pistons, PistonGroupState state)
        {
            if (state == PistonGroupState.Extended)
            {
                // call next group

              //  downPiston4Status.Extend();
            }
            else if (state == PistonGroupState.Retracted)
            {
            }
        }

        private void DownPiston2Status_OnPistonEvent(List<IMyPistonBase> pistons, PistonGroupState state)
        {
            if (state == PistonGroupState.Extended)
            {
                // call next group

                downPiston3Status.Extend();
            }
            else if (state == PistonGroupState.Retracted)
            {
            }
        }

        private void DownPiston1Status_OnPistonEvent(List<IMyPistonBase> pistons, PistonGroupState state)
        {
            if(state == PistonGroupState.Extended)
            {
                // call next group

                downPiston2Status.Extend();
            }
            else if (state == PistonGroupState.Retracted)
            {
            }
        }

        private void UpPistonStatus_OnPistonEvent(List<IMyPistonBase> pistons, PistonGroupState state)
        {
            if (state == PistonGroupState.Retracted) 
            {
                downPiston1Status.Extend(); 
            }
            else if (state == PistonGroupState.Retracted)
            {
            }

        }

        public void Retract()
        {
            float retractVelocity = 0.1f;

            isDrilling = false;

            upPistonStatus.PistonVelocity = retractVelocity;

            downPiston1Status.PistonVelocity = retractVelocity;

            downPiston2Status.PistonVelocity = retractVelocity;

            downPiston3Status.PistonVelocity = retractVelocity;

          //  downPiston4Status.PistonVelocity = retractVelocity;

            upPistonStatus.Extend();

            downPiston1Status.Retract(true);

            downPiston2Status.Retract(true);

            downPiston3Status.Retract(true);

          //  downPiston4Status.Retract();

            ApplyBrake(false);

            DisplayIdleMessage();
        }
        public void Extend()
        {
            ApplyBrake(true);

            float extendVelocity = 0.01f;

            isDrilling = true;

            upPistonStatus.PistonVelocity = extendVelocity;

            upPistonStatus.LastStateThrown = PistonGroupState.Extended;

            downPiston1Status.PistonVelocity = extendVelocity;

            downPiston1Status.LastStateThrown = PistonGroupState.Retracted;

            downPiston2Status.PistonVelocity = extendVelocity;

            downPiston2Status.LastStateThrown = PistonGroupState.Retracted;

            downPiston3Status.PistonVelocity = extendVelocity;

            downPiston3Status.LastStateThrown = PistonGroupState.Retracted;

          //  downPiston4Status.PistonVelocity = extendVelocity;

          //  downPiston4Status.LastStateThrown = PistonGroupState.Retracted;

            upPistonStatus.Retract(true);
        }

        /// <summary>
        /// Attempts to set the handbrake on the suspension
        /// </summary>
        /// <param name="state"></param>
        public void ApplyBrake(bool state)
        {
            foreach (var wheel in Wheels)
                wheel.Brake = state;
        }
        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from.
            // 
            // The method itself is required, but the arguments above
            // can be removed if not needed.
            string[] args = argument.ToLower().Split(' ');

            switch(args[0])
            {
                case "extend":
                    {
                        lcdOutput.WriteText("", false);

                        Extend();
                    }
                    break;
                case "retract":
                    {
                        lcdOutput.WriteText("", false);

                        Retract();
                    }
                    break;
                case "stop":
                    Stop();
                    break;
                case "turnon":
                    TurnOn();
                    break;
                case "turnoff":
                    TurnOff();
                    break;
                case "brake":
                    {
                        // on or off ?
                        if(args.Length >= 2)
                        {
                            switch(args[1])
                            {
                                case "on":
                                    ApplyBrake(true);
                                    break;
                                case "off":
                                    ApplyBrake(false);
                                    break;
                            }
                        }
                    }
                    return;
            }

            upPistonStatus.Update();

            downPiston1Status.Update();

            downPiston2Status.Update();

            downPiston3Status.Update();

           // downPiston4Status.Update();

            // Display Percentage of drilling

            if(isDrilling)
                WriteLine(string.Format("Drilling : {0:00.0}% complete.", GetPercentageComplete()), false);
        }

        public float GetPercentageComplete()
        {
            float currPos = 0.0f;
            // Up is backwards

            currPos += upPistonStatus.CurrentReversedPosition;

            currPos += downPiston1Status.CurrentPosition;

            currPos += downPiston2Status.CurrentPosition;

            currPos += downPiston3Status.CurrentPosition;

           // currPos += downPiston4Status.CurrentPosition;

            currPos += 2.5f * 2; // rotor, 1 conveyor

            return (currPos / Depth) * 100.0f;
        }
        public void Write(string value, bool append = true)
        {
            lcdOutput.WriteText(value, append);
        }
        public void WriteLine(string value, bool append = true)
        {
            lcdOutput.WriteText(value + Environment.NewLine, append);
        }
        public void Stop()
        {
            // Stops this script from running after this update
            Runtime.UpdateFrequency = UpdateFrequency.None;

            return;
        }
        public void TurnOn()
        {
            foreach (var drill in Drills)
                drill.ApplyAction("OnOff_On");

            foreach (var upPiston in VerticalUPPistons)
                upPiston.ApplyAction("OnOff_On");

            foreach (var downPiston in VerticalDownPistons1)
                downPiston.ApplyAction("OnOff_On");

            foreach (var downPiston in VerticalDownPistons2)
                downPiston.ApplyAction("OnOff_On");

            foreach (var downPiston in VerticalDownPistons3)
                downPiston.ApplyAction("OnOff_On");

          //  foreach (var downPiston in VerticalDownPistons4)
             //   downPiston.ApplyAction("OnOff_On");
        }
        public void TurnOff()
        {
            foreach (var drill in Drills)
                drill.ApplyAction("OnOff_Off");

            foreach (var upPiston in VerticalUPPistons)
                upPiston.ApplyAction("OnOff_Off");

            foreach (var downPiston in VerticalDownPistons1)
                downPiston.ApplyAction("OnOff_Off");

            foreach (var downPiston in VerticalDownPistons2)
                downPiston.ApplyAction("OnOff_Off");

            foreach (var downPiston in VerticalDownPistons3)
                downPiston.ApplyAction("OnOff_Off");

           // foreach (var downPiston in VerticalDownPistons4)
            //    downPiston.ApplyAction("OnOff_Off");
        }

        /// <summary>
        /// Finds a group of blocks by name and type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<T> FindBlockGroup<T>(string name) where T: class
        {
            List<T> result = new List<T>();

            var group = GridTerminalSystem.GetBlockGroupWithName(name);

            group.GetBlocksOfType(result);

            return result;
        }

        /// <summary>
        /// Our pistons status class, it monitors the state of the pistons
        /// </summary>
        public class PistonGroupStatus
        {
            public List<IMyPistonBase> Pistons;

            public delegate void PistonEvent(List<IMyPistonBase> pistons, PistonGroupState state);

            public event PistonEvent OnPistonEvent;

            public float PistonVelocity = 0.1f;

            public string GroupName;
            
            public PistonGroupState LastStateThrown = PistonGroupState.None;

            IMyTextPanel LCD;

            public PistonGroupStatus(string groupName, List<IMyPistonBase> pistons, IMyTextPanel lcd)
            {
                this.GroupName = groupName;

                this.Pistons = pistons;

                this.LCD = lcd;
            }

            public void Update()
            {
                float currentPos = 0.0f;

                foreach(var piston in Pistons)
                {
                    currentPos += piston.CurrentPosition;
                }

                if (currentPos <= 0.0f && LastStateThrown != PistonGroupState.Retracted)
                {
                    LastStateThrown = PistonGroupState.Retracted;

                    OnPistonEvent?.Invoke(Pistons, PistonGroupState.Retracted);
                }
                else if (currentPos >= Pistons.Count * 2.0f && LastStateThrown != PistonGroupState.Extended)
                {
                    LastStateThrown = PistonGroupState.Extended;

                    OnPistonEvent?.Invoke(Pistons, PistonGroupState.Extended);
                }
            }

            public void Extend()
            {
                if (Pistons == null) return;

                if (CurrentPosition == 0.0f)
                    LastStateThrown = PistonGroupState.Retracted;

                //LastStateThrown = PistonGroupState.None;

                foreach (var piston in Pistons)
                {
                    piston.Velocity = PistonVelocity;
                }
            }

            public void Retract(bool isSmallGrid)
            {
                if (Pistons == null) return;

                float extended = 0.0f;

                if (isSmallGrid)
                    extended = 2f;
                else extended = 10.0f;

                if (CurrentPosition >= Pistons.Count * extended)
                    LastStateThrown = PistonGroupState.Extended;

               // LastStateThrown = PistonGroupState.None;

                foreach (var piston in Pistons)
                {
                    piston.Velocity = -PistonVelocity;
                }
            }
            public float CalculateDepth(bool isSmallGrid = false)
            {
                if (isSmallGrid)
                    return Pistons.Count * 2f;
                else return Pistons.Count * 10f;
            }
            public float CurrentPosition
            {
                get
                {
                    float currPos = 0.0f;

                    foreach (var piston in Pistons)
                        currPos += piston.CurrentPosition;

                    return currPos;
                }
            }

            public float CurrentReversedPosition
            {
                get
                {
                    float currPos = Pistons.Count * 2f;//10.0f;

                    foreach (var piston in Pistons)
                        currPos -= piston.CurrentPosition;

                    return currPos;
                }
            }

            public bool IsRetracted
            {
                get { return CurrentPosition == 0.0f; }
                   
            }
            public bool IsExtendedLargeGrid
            {
                get { return CurrentPosition == Pistons.Count * 10; }
            }
            public bool IsExtendedSmallGrid
            {
                get { return CurrentPosition == Pistons.Count * 2; }
            }
        }

        public enum PistonGroupState
        {
            None,
            Complete,
            Extended,
            Retracted
        }
        #endregion
    }
}
