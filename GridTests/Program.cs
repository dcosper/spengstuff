﻿using GridTests.Core;
using System;

namespace GridTests
{
    class Program
    {
        static ConsoleManager _consoleManager;

        static void Main(string[] args)
        {
            _consoleManager = new ConsoleManager();

            _consoleManager.Listen();
        }
    }
}
