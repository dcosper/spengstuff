﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GridTests.Core
{
    public class ConsoleManager
    {
        public bool isRunning = true;

        GridProgram gridProgram;

        public void Listen()
        {
            while (isRunning)
            {
                PrintInColor(":>", ConsoleColor.Green);
                string input = Console.ReadLine();
                if (input != string.Empty)
                    ProcessCommand(input);
            }
        }

        public void ProcessCommand(string command)
        {
            string[] cmds = command.Split(' ');

            switch (cmds[0].ToLower())
            {
                case "a":
                    {
                        gridProgram = new GridProgram();

                    }
                    break;
                case "up":
                    {
                        gridProgram.Main("up", Sandbox.ModAPI.Ingame.UpdateType.Script);
                    }
                    break;
                case "down":
                    {
                        gridProgram.Main("down", Sandbox.ModAPI.Ingame.UpdateType.Script);
                    }
                    break;
                default:
                    PrintLineInColor(string.Format("No Known Command {0} !!", cmds[0]), ConsoleColor.Red);
                    break;
            }
        }

        #region Tools
        void PrintInColor(string value, ConsoleColor color)
        {
            ConsoleColor prior = Console.ForegroundColor;

            Console.ForegroundColor = color;

            Console.Write(value);

            Console.ForegroundColor = prior;
        }
        void PrintLineInColor(string value, ConsoleColor color)
        {
            ConsoleColor prior = Console.ForegroundColor;

            Console.ForegroundColor = color;

            Console.WriteLine(value);

            Console.ForegroundColor = prior;
        }
        #endregion
    }
}
