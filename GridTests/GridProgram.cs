﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GridTests
{
    public class GridProgram : MyGridProgram
    {
        #region CODE
        // tag to look for lcd

        string _tag = "[LCD]";

        PageManager _pageManager = null;

        IMyTextSurface _lcd = null;

        public GridProgram()
        {
            _pageManager = new PageManager();

            FindLCD();

            Page page1 = new Page();

            page1.AddLine("Welcome to LCDOutput", null , false);

            page1.AddLine("_________________________", null, false);

            page1.AddLine("One very long ass line goes here", new Action(() => { _lcd.WriteText("YEAH"); }));

            page1.AddLine("Two", new Action(() => { }));

            page1.AddLine("Three", new Action(() => { }));

            page1.AddLine("Four", new Action(() => { }));

            page1.AddLine("Five", new Action(() => { }));

            page1.AddLine("Six", new Action(() => { }));
            page1.AddLine("Seven", new Action(() => { }));
            page1.AddLine("Eight", new Action(() => { }));
            page1.AddLine("Nine", new Action(() => { }));
            page1.AddLine("Ten", new Action(() => { }));
            _pageManager.AddPage(page1);

            _pageManager.RedrawScreen();
        }

        public void Main(string argument, UpdateType updateSource)
        {
            string[] args = argument.ToLower().Split(' ');

            switch (args[0])
            {
                case "up":
                    {
                        FindLCD();

                        if (_lcd != null)
                            _pageManager.SetLCD(_lcd);

                        _pageManager.SelectedPage.SelectPreviousLine();

                        _pageManager.RedrawScreen();
                    }
                    break;
                case "down":
                    {
                        FindLCD();

                        if (_lcd != null)
                            _pageManager.SetLCD(_lcd);

                        _pageManager.SelectedPage.SelectNextLine();

                        _pageManager.RedrawScreen();
                    }
                    break;
                case "apply":
                    {
                        FindLCD();

                        if (_lcd != null)
                            _pageManager.SetLCD(_lcd);

                        _pageManager.SelectedPage.Apply();
                        
                        _pageManager.RedrawScreen();
                    }
                    break;
                default:
                    break;
            }
        }

        public void FindLCD()
        {
            if (_lcd != null)
                return;

            List<IMyTerminalBlock> taggedBlocks = FindBlocksWithTag();

            foreach (var block in taggedBlocks)
            {
                if (block is IMyTextSurface)
                {
                    _lcd = block as IMyTextSurface;

                    _lcd.TextPadding = 5.0f;

                    _pageManager.SetLCD(_lcd);

                    return;
                }
            }
        }
        public List<IMyTerminalBlock> FindBlocksWithTag()
        {
            List<IMyTerminalBlock> result = new List<IMyTerminalBlock>();

            List<IMyTerminalBlock> temp = new List<IMyTerminalBlock>();

            GridTerminalSystem.GetBlocks(temp);

            foreach (var block in temp)
            {
                if (block.DisplayNameText.Contains(_tag))
                    result.Add(block);
            }

            return result;
        }
        // Helper classes

        public class PageManager : MyGridProgram
        {
            public List<Page> Pages { get; set; }

            private int _currentIndex = 0;

            IMyTextSurface _lcd;

            public Page SelectedPage { get; set; }

            public PageManager()
            {
                Pages = new List<Page>();
            }

            public void SetLCD(IMyTextSurface lcdOutput)
            {
                _lcd = lcdOutput;
            }

            public Page DisplayNextPage()
            {
                if (Pages == null)
                    return null;

                if (Pages.Count == 0)
                    return null;

                if ((_currentIndex + 1) > (Pages.Count -1))
                    _currentIndex = 0;
                else _currentIndex++;

                Page nextPage = Pages[_currentIndex];

                SelectedPage = nextPage;

                return nextPage;
            }

            public Page GetPreviousPage()
            {
                if (Pages == null)
                    return null;

                if (Pages.Count == 0)
                    return null;

                if ((_currentIndex - 1) < 0)
                    _currentIndex = (Pages.Count - 1);
                else _currentIndex--;

                Page nextPage = Pages[_currentIndex];

                SelectedPage = nextPage;

                return nextPage;
            }

            public void AddPage(Page page)
            {
                Pages.Add(page);

                if (Pages.Count == 1)
                    SelectedPage = Pages[0];
            }

            public void RedrawScreen()
            {
                if (_currentIndex < 0 || _currentIndex > Pages.Count)
                    return;

                SelectedPage = Pages[_currentIndex];

                if (SelectedPage != null)
                    WriteDataOutput(SelectedPage);
            }

            public void WriteDataOutput(Page page)
            {
                if (page == null)
                    return;

                if (_lcd != null)
                    _lcd.WriteText(page.ToString());

                Echo(page.ToString());
            }
        }

        /// <summary>
        /// The page class, each page can hold 12 lines which may have 27 characters per line
        /// </summary>
        public class Page
        {
            public List<LCDLine> Lines = new List<LCDLine>();

            private int _selectedLineIndex = 0;

            public LCDLine Selected { get; set; }

            public delegate void SelectedLineChange(int index);

            public event SelectedLineChange SelectedLineChangedEvent;

            public void AddLine(string data, Action lineAction, bool canSelect = true)
            {
                if (Lines.Count == 12)
                    return;

                Lines.Add(new LCDLine(data, lineAction, canSelect));

                ReIndexLines();
            }

            public void SelectNextLine()
            {
                int max = GetMaxSelectableItems() -1;

                if ((_selectedLineIndex + 1) > max)
                    _selectedLineIndex = 0;
                else _selectedLineIndex++;

                Selected = Lines[_selectedLineIndex];

                SelectedLineChangedEvent?.Invoke(_selectedLineIndex);
            }

            public void SelectPreviousLine()
            {
                if ((_selectedLineIndex - 1) < 0)
                    _selectedLineIndex = GetMaxSelectableItems() -1;
                else _selectedLineIndex--;

                Selected = Lines[_selectedLineIndex];

                SelectedLineChangedEvent?.Invoke(_selectedLineIndex);
            }

            public void Apply()
            {
                for(int i = 0;i<Lines.Count;i++)
                {
                    if(Lines[i].Index == _selectedLineIndex)
                    {
                        Lines[i].PerformLineAction();
                        return;
                    }
                }
            }

            public override string ToString()
            {
                ReIndexLines();

                StringBuilder result = new StringBuilder();

                for (int i = 0; i < Lines.Count; i++)
                {
                    if (Lines[i] == null)
                        continue;

                    if (Lines[i].CanSelect && Lines[i].Index == _selectedLineIndex)
                        result.AppendLine(">" + Lines[i].ToString());
                    else result.AppendLine(Lines[i].ToString());
                }

                return result.ToString();
            }

            void ReIndexLines()
            {
                int index = 0;

                foreach(var line in Lines)
                {
                    if(line.CanSelect)
                    {
                        line.Index = index;

                        index++;
                    }
                }
            }
            int GetMaxSelectableItems()
            {
                int maxSelectable = 0;

                foreach (var line in Lines)
                {
                    if (line.CanSelect)
                        maxSelectable++;
                }

                return maxSelectable;
            }
        }

        /// <summary>
        /// The line used to hold the data to displat to screen and the action to perform on apply
        /// </summary>
        public class LCDLine
        {
            public List<char> Line = new List<char>();

            public bool CanSelect = true;

            public int Index = -1;

            private Action _lineAction;

            private int _maxLineLength = 27;

            public LCDLine(string data, Action action, bool canSelect = true)
            {
                _lineAction = action;

                CanSelect = canSelect;

                ConvertString(data);
            }

            public override string ToString()
            {
                return new string(Line.ToArray());
            }

            public void PerformLineAction()
            {
                _lineAction?.Invoke();
            }

            /// <summary>
            /// Converts the string into a char array and enforces the data length
            /// will trunicate any string longer than allowed
            /// </summary>
            /// <param name="data"></param>
            void ConvertString(string data)
            {
                Line.Clear();

                char[] c = data.ToCharArray();

                for (int i = 0; i < c.Length; i++)
                {
                    if (i <= _maxLineLength)
                        Line.Add(c[i]);
                }
            }
        }
        #endregion 
    }
}
